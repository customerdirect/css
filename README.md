# Customer Direct's CSS / Sass Styleguide

_A mostly reasonable approach to CSS and Sass_

## Table of Contents

##

[TOC]

##

 

## Installation

Copy and paste the `.sass-lint.yml` file into your project. :boom:


## Terminology

### Rule declaration

A "rule declaration" consists of a selector (or a group of selectors) with an accompanying group of properties. An example:


```
#!css

.link {
  font-size: 14px;
  text-decoration: none;
}
```


### Selectors

Selectors determine which elements in the DOM tree will be styled by the defined properties. Here are some example of selectors:

```
#!css

.awesome-class { /* ... */ }

[awesome-attribute] { /* ... */ }
```

### Properties

Properties are what give the selected elements of a rule declaration their style. Properties are key-value pairs. Property declarations look like this:

```
#!css

{
  background-color: #000;
  float: left
}
```

## CSS

### Formatting

* Use soft tabs (2 spaces) for indentation
* Prefer dashes over camelCasing in class names.
  * Underscores and PascalCasing are okay if you are using (BEM)[https://css-tricks.com/bem-101/]
* Avoid ID selectors
* Put a space before the opening brace `{` in rule declarations
* Put a space after, but not before, the `:` character.
* Put closing braces `}` of rule declarations on a new line
* Put blank lines between rule declarations

##### Bad

```
#!scss

.no{
    border-radius:50%;
    border:2px solid white; }
.noo, .nope, .not_good {
    // ...
}
#lol-please-dont {
  // ...
}
```

##### Good

```
#!scss

.yes {
  border-radius: 1em;
  border: 2px solid white;
}

.good,
.grand,
.wonderful {
  // ...
}
```

#### Comments

* Use line comments `//` over block comments
* Write detailed comments for code that isn't self-documenting:
  * Uses of z-index
  * Compatibility or browser-specific hacks

### Border

Use `0` instead of `none` to specify that a style has no border.

##### Bad

```
#!css

.sharpie {
  border: none;
}
```

##### Good

```
#!css

.sharpie {
  border: 0;
}
```


### ID selectors

While it is possible to select elements by ID in CSS, it should be considered an anti-pattern. ID selectors introduce an unnecessarily high level of specificity to your rule declarations, and they are not reusable.

For more on the subject, review [CSS Wizardry's article](http://csswizardry.com/2014/07/hacks-for-dealing-with-specificity/).


## Sass

### Syntax

Don't use the original `.sass` syntax, instead use the `.scss` syntax.

### Variables

Prefer dash-cased variable names (e.g. `$wow-cool-var`) over camelCased or underscore_cased names.

### Mixins

Like functions, mixins should only be used to DRY up your code, add clarity, or abstract complexity. Just note that if you are not compressing your payload, this may contribute to unnecessary code duplication in the resulting styles.

### Extends

## Nested selectors

### Don't nest selectors more than three levels deep!

```
#!scss

.container {
  .content {
    .item {
      // Stop.
    }
  }
}
```

When selectors become this long, you're likely writing CSS that is:
* Strongly coupled to the HTML (fragile)-- OR --
* Overly specific -- OR --
* Not reusable

### Never nest ID selectors!

If you are writing well formed HTML and CSS, you should **never** need to do this. You need to revisit your markup, or figure out why a strong specificity is needed.

## Inline Linting

* Sublime - [SublimeLinter-sass-lint](https://github.com/skovhus/SublimeLinter-contrib-sass-lint)
* VIM - [vim-sass-lint](http://vimawesome.com/plugin/vim-sass-lint)
* Atom - [linter-sass-lint](https://atom.io/packages/linter-sass-lint)
* RubyMine - [Sass Lint](https://plugins.jetbrains.com/plugin/8171?pr=webStorm)

## Maintenance

This document and `.scss-lint.yml` configuration is maintained by the wizards at Customer Direct, LLC in St. Louis, MO.